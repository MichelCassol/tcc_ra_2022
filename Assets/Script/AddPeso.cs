using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddPesso : MonoBehaviour
{
    public float peso;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Moved)
            {
                transform.position += (Vector3)t.deltaPosition;
            }
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.GetComponent<Rigidbody>())
        {
            peso += coll.GetComponent<Rigidbody>().mass;
        }
    }

    private void OnTriggerExit(Collider coll)
    {
        if (coll.GetComponent<Rigidbody>())
        {
            peso -= coll.GetComponent<Rigidbody>().mass;
        }
    }
}
